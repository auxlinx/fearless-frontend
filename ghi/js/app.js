function createCard(name, description, pictureUrl, starts, ends, conferenceCity, conferenceLocation, conferenceLocationState) {
      // Create new Date objects from the starts and ends strings
  const startDate = new Date(starts);
  const endDate = new Date(ends);

  // Format the dates to MM-DD-YYYY
  const formattedStarts = startDate.toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' });
  const formattedEnds = endDate.toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' });
    
    return `
    <div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); margin: 10px; width: 18rem;">
        <img src="${pictureUrl}" class="card-img-top" style="width: 18em; height: 18em;">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${conferenceLocation} - ${conferenceCity} ,${conferenceLocationState}  </h6>
          <p class="card-text">${description}</p>
          <p class="card-text">${formattedStarts} - ${formattedEnds}</p>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json()
        let row;
        for (let i = 0; i < data.conferences.length; i++) {
            // this makes every row three cards
            if (i % 3 === 0) {
                row = document.createElement('div');
                row.className = 'row';
                document.querySelector('.container').appendChild(row);
              }
        // const columns = document.querySelectorAll('.col')
        const conference = data.conferences[i]
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const conferenceCity = details.conference.location.city;
                const conferenceLocation = details.conference.location.name;
                const conferenceLocationState = details.conference.location.state;
                const startDate = details.conference.starts;
                const endsDate = details.conference.ends;
                const html = createCard(title, description, pictureUrl, startDate, endsDate, conferenceCity,conferenceLocation, conferenceLocationState);
                //  this creates a div for the cards
                const column = document.createElement('div');
                // const column = document.querySelector('.col');
                // this makes a column for the cards
                column.className = 'col';
                column.innerHTML += html;
                //  this makes the column a child of the div
                row.appendChild(column);
                console.log(details)
          }
          
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e);
    }
  
  });

// This works
// function createCard(name, description, pictureUrl) {
//     return `
//       <div class="card">
//         <img src="${pictureUrl}" class="card-img-top">
//         <div class="card-body">
//           <h5 class="card-title">${name}</h5>
//           <p class="card-text">${description}</p>
//         </div>
//       </div>
//     `;
//   }

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();
//         for (let i = 0; i < data.conferences.length; i++) {
//         const columns = document.querySelectorAll('.col')
//         const conference = data.conferences[i]
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//                 const details = await detailResponse.json();
//                 const title = details.conference.name;
//                 const description = details.conference.description;
//                 const pictureUrl = details.conference.location.picture_url;
//                 const html = createCard(title, description, pictureUrl);
//                 const column = document.querySelector('.col');
//                 column.innerHTML += html;
//           }
          
//         }
  
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//       console.error(e);
//     }
  
//   });


// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();
  
//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;
//         const descriptionTag = document.querySelector('.card-text');
//         descriptionTag.innerHTML = conference.description;
//         const imageTag = document.querySelector('.card-img-top');
//         imageTag.src = details.conference.location.picture_url;
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           console.log(details);
//         }
  
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }
  
//   });
